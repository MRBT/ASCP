#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <boost/asio.hpp>

#include "lib/args.h"
#include "lib/server_examples.h"

using boost::asio::ip::tcp;

using std::cout;
using std::endl;
using std::string;

int main(int argc, char **argv) {
    try {

        ParsedArguments args;

        int parsed_args_rc = parse_pargs(argc, argv, &args);
        if (parsed_args_rc != 0)
            return parsed_args_rc;

        // We need to create a server object to accept incoming client connections.
        boost::asio::io_service io_service;

        // The io_service object provides I/O services, such as sockets,
        // that the server object will use.

        tcp_server server(io_service, args.host, args.port, &ServerExamples::timestamp_protobuf_processor);
        //tcp_server server(io_service, args.host, args.port);

        std::cout << "Server listening on " << args.host << ':' << args.port << std::endl;

        // Run the io_service object to perform asynchronous operations.
        io_service.run();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
