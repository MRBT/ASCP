//
// Created by jirislav on 13.5.18.
//

#include "tcp_server.h"

#ifdef SERVER_STATISTICS
#include <boost/format.hpp>
#endif

using namespace std::chrono;

using boost::asio::ip::tcp;


void session::start() {

    do_read();
}

void session::do_read() {
    auto self(shared_from_this());

    // First read the size of the whole message
    boost::asio::async_read(
            socket_,
            request_size.prepare(SIZEOF_HEADER_SIZE),
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (!ec) {
                    if (length != SIZEOF_HEADER_SIZE) {

                        std::ostringstream oss;
                        oss << "Query HeaderSize contains only " << length << " bytes out of total "
                            << SIZEOF_HEADER_SIZE << " bytes expected!\n";

                        throw std::underflow_error(oss.str());
                    }

                    // Determine current time
                    double received_at = duration_cast<microseconds>(
                            system_clock::now().time_since_epoch()
                    ).count() / 1e6;

                    HeaderSize request_size_descriptor = get_header_size(&request_size);

#ifdef SERVER_STATISTICS
                    // Determine current time
                    double after_header_size = duration_cast<microseconds>(
                            system_clock::now().time_since_epoch()
                    ).count() / 1e6;
#endif

                    // Now we shall rather use sync read (we're already inside a reading session)
                    length = boost::asio::read(
                            socket_,
                            request.prepare(request_size_descriptor),
                            ec
                    );

                    if (length != request_size_descriptor) {

                        std::ostringstream oss;
                        oss << "Query contains only " << length << " bytes out of total "
                            << request_size_descriptor << " bytes expected!\n";

                        throw std::underflow_error(oss.str());
                    } else if (!ec) {

#ifdef SERVER_STATISTICS
                        // Determine current time
                        double after_whole_read = duration_cast<microseconds>(
                                system_clock::now().time_since_epoch()
                        ).count() / 1e6;
#endif

                        request.commit(length);

                        // Lol? Don't know why minus 1 :D :D .. don't ask me
                        request.consume(SIZEOF_HEADER_SIZE - 1);

#ifdef SERVER_STATISTICS
                        // Determine current time
                        double before_callback = duration_cast<microseconds>(
                                system_clock::now().time_since_epoch()
                        ).count() / 1e6;
#endif

                        (*callback_)(&received_at, &request, length, &response);

#ifdef SERVER_STATISTICS
                        // Determine current time
                        double after_callback = duration_cast<microseconds>(
                                system_clock::now().time_since_epoch()
                        ).count() / 1e6;

                        std::cout
                                << " ----------------------------------------------------------------------\n"
                                << " --- head("
                                << boost::format("%05.2f") % ((after_header_size - received_at) * 1000.0)
                                << "ms) read("
                                << boost::format("%05.2f") % ((after_whole_read - after_header_size) * 1000.0)
                                << "ms) commit("
                                << boost::format("%05.2f") % ((before_callback - after_whole_read) * 1000.0)
                                << "ms) callback("
                                << boost::format("%05.2f") % ((after_callback - before_callback) * 1000.0)
                                << "ms) --- \n"
                                << " ----------------------------------------------------------------------\n";
#endif

                        do_write();
                    } else {
                        std::cout << "Client disconnected: "
                                  << endpoint_->address() << ':' << endpoint_->port()
                                  << '\n';
                    }

                } else {

                    std::cout << "Client disconnected: "
                              << endpoint_->address() << ':' << endpoint_->port()
                              << '\n';
                }
            }
    );
}

void session::do_write() {
    auto self(shared_from_this());

    size_t how_much = response.size();
    if (how_much > std::numeric_limits<HeaderSize>::max()) {
        throw std::overflow_error(
                "Response size is greater than what this protocol can do!"
        );
    }

    set_header_size(&response_size, static_cast<HeaderSize>(response.size()));

    // Concatenate two the buffers together
    std::vector<boost::asio::streambuf::const_buffers_type> buffers;
    buffers.push_back(response_size.data());
    buffers.push_back(response.data());

    // write the response size
    boost::asio::async_write(
            socket_,
            buffers,
            [this, self](boost::system::error_code ec1, std::size_t length) {
                if (!ec1) {
                    do_read();
                } else {
                    std::cout << "Connection closed while sending size of the response.\n";
                }
            }
    );
}

void tcp_server::do_accept() {
    acceptor_.async_accept(
            socket_,
            [this](boost::system::error_code ec) {
                if (!ec) {

                    tcp::endpoint endpoint = socket_.remote_endpoint(ec);
                    if (!ec) {

                        std::cout << "Got new connection from: " << endpoint.address() << ':' << endpoint.port()
                                  << '\n';

                        std::make_shared<session>(std::move(socket_), callback_, &endpoint)->start();
                    } else {
                        std::cout << "Connection closed by client while connecting together.\n";
                    }

                } else {
                    std::cout << "Connection closed by client while connecting together.\n";
                }

                do_accept();
            }
    );
}


