//
// Created by jirislav on 14.5.18.
//

#ifndef ASCP_TCP_SERVER_H
#define ASCP_TCP_SERVER_H

// Uncomment this to start doing statistics on server
// #define SERVER_STATISTICS

#include <cstdlib>
#include <iostream>
#include <thread>
#include <utility>
#include <boost/asio.hpp>
#include "../proto/message.pb.h"
#include "constants.h"

#include <unistd.h>

using boost::asio::ip::tcp;

using Callback = std::function<void(
        double *,
        boost::asio::streambuf *,
        std::size_t,
        boost::asio::streambuf *
)>;

class session
        : public std::enable_shared_from_this<session> {

public:

    session(
            tcp::socket socket,
            Callback *callback,
            tcp::endpoint *endpoint
    ) : socket_(std::move(socket)),
        callback_(callback),
        endpoint_(endpoint){}

    void start();

    static HeaderSize get_header_size(boost::asio::streambuf *stream_buf_header) {

        stream_buf_header->commit(SIZEOF_HEADER_SIZE);

        std::string to_read(
                (std::istreambuf_iterator<char>(stream_buf_header)),
                std::istreambuf_iterator<char>()
        );

        HeaderSize value = *reinterpret_cast<HeaderSize *>(&to_read[0]);

        long available_pages = sysconf(_SC_AVPHYS_PAGES);
        long page_size = sysconf(_SC_PAGE_SIZE);
        long available_ram = available_pages * page_size;
        if (available_ram < value) {

            std::ostringstream oss;
            oss << "Request is too big! Not enough available RAM to process it.\n"
                << "Required:  " << value << '\n'
                << "Available: " << available_ram << '\n';

            throw std::runtime_error(oss.str());
        }

        return value;
    }

    static void set_header_size(boost::asio::streambuf *stream_buf_header, HeaderSize size) {
        stream_buf_header->prepare(SIZEOF_HEADER_SIZE);

        std::string to_write(reinterpret_cast<const char *>(&size));
        std::iostream write(stream_buf_header);
        write << to_write;

        stream_buf_header->commit(SIZEOF_HEADER_SIZE);
    }

private:
    void do_read();

    void do_write();

    Callback *callback_;
    tcp::socket socket_;
    tcp::endpoint *endpoint_;

    boost::asio::streambuf request_size;
    boost::asio::streambuf request;

    boost::asio::streambuf response_size;
    boost::asio::streambuf response;
};

class tcp_server {

public:

    /**
     * C'tor with default server logic where the response on a request is the request itself.
     *
     * @param io_service
     * @param host
     * @param port
     */
    tcp_server(
            boost::asio::io_service &io_service,
            std::string &host,
            uint16_t port
    ) : tcp_server(io_service, host, port, &default_logic) {};

    /**
     * C'tor with custom callback
     *
     * @param io_service
     * @param host
     * @param port
     * @param callback
     */
    tcp_server(
            boost::asio::io_service &io_service,
            std::string &host,
            uint16_t port,
            Callback *callback
    ) : acceptor_(io_service, tcp::endpoint(boost::asio::ip::address::from_string(host), port)),
        socket_(io_service),
        callback_(callback) {
        do_accept();
    };

    /**
     *  Simple default server logic setting the response to be the request itself.
     *
     * @param request
     * @param req_length
     * @param response
     */
    Callback default_logic = [](
            double *received_at,
            boost::asio::streambuf *request,
            std::size_t req_length,
            boost::asio::streambuf *response
    ) {

        response->commit(
                boost::asio::buffer_copy(
                        response->prepare(req_length),
                        request->data()
                )
        );
    };

private:
    void do_accept();

    Callback *callback_;
    tcp::acceptor acceptor_;
    tcp::socket socket_;
};

#endif //ASCP_TCP_SERVER_H
