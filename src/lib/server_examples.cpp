//
// Created by jirislav on 14.5.18.
//

#include <boost/asio.hpp>
#include "server_examples.h"
#include "../proto/message.pb.cc"

#ifdef SERVER_STATISTICS

#include <boost/format.hpp>

#endif

using boost::asio::ip::tcp;
using namespace std::chrono;
using namespace google::protobuf::io;

using std::cout;
using std::endl;
using std::string;

Callback ServerExamples::timestamp_protobuf_processor = [](
        const double *received_at,
        boost::asio::streambuf *request,
        std::size_t req_length,
        boost::asio::streambuf *response
) {

#ifdef SERVER_STATISTICS
    // Determine current time
    double st0 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif

    ascp::Data input_data;


    std::istream in(request);

#ifdef SERVER_STATISTICS
    // Determine current time
    double st1 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif
    bool failed = input_data.ParseFromIstream(&in);

#ifdef SERVER_STATISTICS
    // Determine current time
    double st2 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif

    if (failed) {
        throw std::runtime_error("PROTOCOL ERROR: Failed to deserialize received data!");
    }

    // Alter the data so that it is the same except we put there current time
    ascp::Data output_data;


    google::protobuf::RepeatedPtrField<string> data(input_data.bytes().begin(), input_data.bytes().end());

#ifdef SERVER_STATISTICS
    // Determine current time
    double st3 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif
    output_data.mutable_bytes()->Swap(&data);

#ifdef SERVER_STATISTICS
    // Determine current time
    double st4 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif

    output_data.set_type(input_data.type());

    // Determine current time
    double sent_at = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;

    output_data.set_received_at(*received_at);
    output_data.set_sent_at(sent_at);


#ifdef SERVER_STATISTICS
    // Determine current time
    double st5 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif

    // Serialize the data so it can be sent back :)
    std::ostream out(response);
    output_data.SerializeToOstream(&out);

#ifdef SERVER_STATISTICS
    // Determine current time
    double st6 = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;
#endif

    // That's all !
    response->commit(response->size());

#ifdef SERVER_STATISTICS
    // Determine current time
    double st_end = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;

    std::cout
            << " ----------------------------------------------------------------------------------------------------------------------------------------\n"
            << " --- stream2string("
            << boost::format("%05.2f") % ((st1 - st0) * 1000.0)
            << "ms) parseStr("
            << boost::format("%05.2f") % ((st2 - st1) * 1000.0)
            << "ms) rptPtr("
            << boost::format("%05.2f") % ((st3 - st2) * 1000.0)
            << "ms) mutByteSw("
            << boost::format("%05.2f") % ((st4 - st3) * 1000.0)
            << "ms) setters("
            << boost::format("%05.2f") % ((st5 - st4) * 1000.0)
            << "ms) msg2stream("
            << boost::format("%05.2f") % ((st6 - st5) * 1000.0)
            << "ms) commit("
            << boost::format("%05.2f") % ((st_end - st6) * 1000.0)
            << "ms) --- \n"
            << " ----------------------------------------------------------------------------------------------------------------------------------------\n";
#endif
};
