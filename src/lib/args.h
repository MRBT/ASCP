//
// Created by jirislav on 14.5.18.
//

#ifndef ASCP_ARGS_H
#define ASCP_ARGS_H

#include <string>

struct ParsedArguments {
    std::string filename = "";
    std::string host = "0.0.0.0";
    uint16_t port = 8000;
};

typedef struct ParsedArguments ParsedArguments;

int parse_pargs(int argc, char **argv, ParsedArguments *args, bool wantFile = false);

#endif //ASCP_ARGS_H
