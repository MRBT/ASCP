//
// Created by jirislav on 14.5.18.
//

#ifndef ASCP_CONSTANTS_H
#define ASCP_CONSTANTS_H

/*
 * The size of the MAX_BYTES_IN_BUFFER determines the maximum length of processing buffer for both the server & client.
 *
 * If you have available RAM, don't hesitate to set it to a very large value - it will be used only if comes very large
 * request.
 *
 */

// 1 MB
// #define MAX_BYTES_IN_BUFFER 1 << 20

// 2,1 MB
// #define MAX_BYTES_IN_BUFFER 1 << 21

// 4,2 MB
// #define MAX_BYTES_IN_BUFFER 1 << 22

// 8,4 MB
// #define MAX_BYTES_IN_BUFFER 1 << 23

// 16,8 MB
// #define MAX_BYTES_IN_BUFFER 1 << 24

// 33,5 MB
// #define MAX_BYTES_IN_BUFFER 1 << 25

// 67,1 MB
// #define MAX_BYTES_IN_BUFFER 1 << 26

typedef uint32_t HeaderSize;

#define SIZEOF_HEADER_SIZE sizeof(HeaderSize)

#endif //ASCP_CONSTANTS_H
