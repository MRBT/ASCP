//
// Created by jirislav on 14.5.18.
//

#include "args.h"

#include <unistd.h>
#include <iostream>
#include <sys/stat.h>

inline bool file_exists(const std::string &name) {
    struct stat buffer{};
    return (stat(name.c_str(), &buffer) == 0);
}

int parse_pargs(int argc, char **argv, ParsedArguments *args, bool wantFile) {

    int c;
    while ((c = getopt(argc, argv, "p:h:f:")) != -1)
        switch (c) {
            case 'p':
                try {
                    args->port = static_cast<uint16_t>(std::stoi(optarg));
                } catch (std::exception const &e) {
                    fprintf(stderr, "Provided port '%s' could not be converted to a number!\n", optarg);
                    return 2;
                }
                break;
            case 'h':
                args->host = optarg;
                break;
            case 'f':
                if (wantFile) {
                    args->filename = optarg;
                    break;
                }
            case '?':
                if (optopt == 'p' || optopt == 'h')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort();
        }

    if (wantFile) {
        if (args->filename.empty()) {
            fprintf(stderr, "You have to provide file using -f FILE.\n");
            return 2;
        }

        if (!file_exists(args->filename)) {
            fprintf(stderr, "Provided file '%s' doesn't exist!\n", args->filename.c_str());
            return 3;
        }
    }
    return 0;
}