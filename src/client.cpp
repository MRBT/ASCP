#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <fstream>
#include <chrono>

// For boost::format
#include <experimental/filesystem>

using boost::asio::ip::tcp;
using namespace std::chrono;

#include "proto/message.pb.cc"
#include "lib/args.h"
#include "lib/constants.h"
#include "lib/tcp_server.h"

typedef uint8_t BYTE;

std::string basename(const std::string &path) {
    auto found = static_cast<unsigned int>(path.find_last_of("/\\"));
    return path.substr(found + 1);
}

int main(int argc, char **argv) {

    ParsedArguments args;

    int parsed_args_rc = parse_pargs(argc, argv, &args, true);
    if (parsed_args_rc != 0)
        return parsed_args_rc;

    // We need to create a server object to accept incoming client connections.
    boost::asio::io_service io_service;
    tcp::socket socket_(io_service);

    try {

        boost::asio::streambuf request_size;
        boost::asio::streambuf request;
        boost::asio::streambuf reply_size;
        boost::asio::streambuf reply;
        ascp::Data response;

        tcp::resolver resolver(io_service);

        boost::asio::connect(
                socket_,
                resolver.resolve(
                        tcp::endpoint(boost::asio::ip::address::from_string(args.host), args.port)
                )
        );






        // open the file:
        std::ifstream file(args.filename, std::ios::binary);

        // Stop eating new lines in binary mode!!!
        file.unsetf(std::ios::skipws);

        // get its size:
        file.seekg(0, std::ios::end);
        std::streampos fileSize = file.tellg();
        file.seekg(0, std::ios::beg);

        std::string image_definition = std::string(
                std::istream_iterator<BYTE>(file),
                std::istream_iterator<BYTE>()
        );


        ascp::Data data;
        data.set_type(ascp::DataType::IMAGE);
        data.add_bytes(image_definition);

        std::ostream ostream(&request);

        // Add timestamp to the request ..
        double sent_at = duration_cast<microseconds>(
                system_clock::now().time_since_epoch()
        ).count() / 1e6;

        data.set_sent_at(sent_at);
        data.SerializeToOstream(&ostream);

        // Send request :)

        session::set_header_size(&request_size, static_cast<HeaderSize>(request.size()));

        // Concatenate two the buffers together
        std::vector<boost::asio::streambuf::const_buffers_type> buffers;
        buffers.push_back( request_size.data() );
        buffers.push_back( request.data() );

        boost::asio::write(socket_, buffers);






        // Receive size of the response first :)
        size_t reply_length = boost::asio::read(socket_, reply_size.prepare(SIZEOF_HEADER_SIZE));

        if (reply_length != SIZEOF_HEADER_SIZE) {

            std::cerr << "Query HeaderSize contains only " << reply_length << " bytes out of total "
                      << SIZEOF_HEADER_SIZE << " bytes expected!\n";

            return 1;
        }

        HeaderSize reply_header_size = session::get_header_size(&reply_size);

        // Now receive the response :)
        reply_length = boost::asio::read(socket_, reply.prepare(reply_header_size));

        if (reply_length != reply_header_size) {
            std::cerr << "Server response contains only " << reply_length << " bytes out of total "
                      << reply_header_size << " bytes expected!\n";
            return 1;
        }

        double returned_at = duration_cast<microseconds>(
                system_clock::now().time_since_epoch()
        ).count() / 1e6;

        reply.commit(reply_length);
        reply.consume(SIZEOF_HEADER_SIZE - 1);


        std::istream reply_istream(&reply);
        std::string definition(
                (std::istreambuf_iterator<char>(&reply)),
                std::istreambuf_iterator<char>()
        );
        bool failed = response.ParseFromString(definition);
        if (failed) {
            throw std::runtime_error("PROTOCOL ERROR: Failed to deserialize received data!");
        }

        double server_received_at = response.received_at();
        double server_sent_at = response.sent_at();

        std::string image_contents;

        int i;
        for (i = 0; i < response.bytes_size(); ++i) {
            image_contents.append(response.bytes(i));
        }

        std::cout << "TOF ("
                  << basename(args.filename)
                  << '/'
                  << image_contents.size() / 1000.0
                  << "kB"
                  << "): \t"
                  << boost::format("%05.2f") % ((returned_at - sent_at) * 1000.0)
                  << " ms (client ->("
                  << boost::format("%05.2f") % ((server_received_at - sent_at) * 1000.0)
                  << "ms)-> server (in) ->("
                  << boost::format("%05.2f") % ((server_sent_at - server_received_at) * 1000.0)
                  << "ms)-> server (out) ->("
                  << boost::format("%05.2f") % ((returned_at - server_sent_at) * 1000.0)
                  << "ms)-> client) "
                  << '\n';

        socket_.close();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        socket_.close();
        return 2;
    };

    return 0;
}