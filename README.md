# Automotive Sensor Communication Protocol (ASCP)

This project aims to provide as fast communication protocol, as possible.

The main goal is to decrease the latency from sensors while driving in a vehicle at high speeds.

### Example problem

Imagine your car current speed is 144 km/h (that is 40 m/s). 
Front camera takes a picture of how it looks in front of the car at the moment & sends that picture to a processing unit, which then decides what to do next.

Now consider the latency for recieving the picture to be an extreme - 100 ms, which means that the processing unit is making a decision based on something that was up to date 4 meters behind the car!

Add another 100 ms between processing unit and the wheels controller and you may now easily recognize that every milisecond of a communication latency is very crucial for a self-driven car to function safely.

### How do we want to accomplish our goal

- First, we need to study the [ROS core system](http://wiki.ros.org/ROS/Tutorials#Beginner_Level) to say hello to the sensoric intercommunication.
- Second, we will try to locate "lag" spots in the code.
- Third, we will try to start writing our own communication protocol (CP), attempting to create faster CP.
  - For now, we're considering to use the Google's [Protocol Buffers](https://developers.google.com/protocol-buffers/docs/overview) for this part.
  - We do also consider to use UDP instead of TCP/IP.


## Benchmarking

There is already completed a benchmark for ROS messaging measuring only the time it takes for certain amount of data to flow from `client` to `server` and back to `client` unchanged to simulate heavy traffic. The data we are sending are images located in `img/` directory.

### Running benchmarks

#### ROS Server/Client application

```bash
cd ros
./run_benchmark.sh  # And that's it !! :)
```

 - You can see that ROS implementation of server/client application is only slightly faster in C++ than in Python
 - Note that development of Python implementation took about 1 hour, but C++ took about 18 hours (you have to count with zero experience with ROS of the programmer)
![image](/uploads/7cf2d33a5676f6cc6f0139d3f775a179/image.png)

#### Google Protobuf benchmark

Build all and start a server:
```bash
./build.sh
./server
```

Then, in another terminal, start series of clients:
```bash
for _ in `seq 1 5`; do ./client -f img/large.jpg; done
for _ in `seq 1 5`; do ./client -f img/medium.jpg; done
```
 - We have found out that Google Protocol Buffer is kinda slow, because only serialization & deserialization of the message took tremendous 26ms! (in case of `img/large.jpg`)
 - So if you subtract this number from the total delay seen in the following benchmark, you find out, we're slightly faster, than ROS :)
 - Note that at the end of the timeline we had for this project we have found out that it would be much better to use [Boost Serialization](https://www.boost.org/doc/libs/1_62_0/doc/html/boost_asio/example/cpp03/serialization/connection.hpp). But there was not much time left to try it out.
![image](/uploads/494c46a45ed8665e2a9318d1fe554249/image.png)
