#!/bin/bash

suggest_this() {

	echo
	echo "Something went wrong, please review https://github.com/google/protobuf/blob/master/src/README.md"
	echo
	exit 2
}

sudo apt-get install autoconf automake cmake g++ libprotobuf-dev libboost1.62-dev || suggest_this
