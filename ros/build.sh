#!/bin/sh

docker-compose build ros-base
docker-compose build ros-opencv
docker-compose build ros-tutorial
docker-compose build ros-benchmark
