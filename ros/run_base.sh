#!/bin/bash

if test -z "`which docker-compose`"; then
    echo "You first have to install docker & docker-compose!"
    echo
    echo " See this link:"
    echo "  - https://docs.docker.com/compose/install/#prerequisites"
    echo
    exit 1
fi

# Allow to get X11 from within the container
xhost +local:ros-base-docker >/dev/null

docker-compose up -d ros-base

echo
echo "Attaching you to the 'ros-base' bash ..."
echo

docker-compose exec ros-base bash
