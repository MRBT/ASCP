#!/bin/bash
#
# This is to be started from within the docker. Use ./run_benchmark.sh from the upper directory!
#
CATKIN_WS=/root/catkin_ws

PACKAGE_NAME=image_benchmark
SERVICE_NAME=image_upload

GENERAL_DEPS="rospy roscpp cv_bridge"
MSG_DEPS="sensor_msgs std_msgs"

init() {
	# Source ROS bundle ..
	source /opt/ros/lunar/setup.bash
	return $?
}

create_package() {
	# Create workspace
	mkdir -p "$CATKIN_WS/src"

	# Create our package
	cd "$CATKIN_WS/src"

	# We want to create it from the scratch again ..
	rm -rf "$PACKAGE_NAME" &>/dev/null
	catkin_create_pkg "$PACKAGE_NAME" $MSG_DEPS $GENERAL_DEPS >/dev/null

	# Build the package from workspace
	cd "$CATKIN_WS"
	catkin_make >/dev/null

	# Source the workspace
	source "$CATKIN_WS/devel/setup.bash"

	return $?
}

create_service() {

	roscd $PACKAGE_NAME
	mkdir srv

	local SERVICE=BounceImage.srv

	cat << EOF > srv/$SERVICE
float64 request_time
sensor_msgs/Image request_image
---
float64 response_time
sensor_msgs/Image response_image
EOF

	sed -i 's,^# add_compile_options,add_compile_options,g' CMakeLists.txt # Enable C++11 :)

	# Add message_generation to the required packages
	sed -i 's,^find_package.*,\0\n  message_generation,g' CMakeLists.txt
	sed -i "s,^catkin_package.*,BEFORE_PACKAGE\\0 CATKIN_DEPENDS $MSG_DEPS $GENERAL_DEPS message_runtime,g" CMakeLists.txt
	sed -i "s,^BEFORE_PACKAGE,BEFORE_GENERATE\\ngenerate_messages( DEPENDENCIES $MSG_DEPS )\\n,g" CMakeLists.txt
	sed -i "s,^BEFORE_GENERATE,add_service_files( FILES $SERVICE )\\n,g" CMakeLists.txt

	sed -i 's,^</package>,  <build_depend>message_generation</build_depend>\n  <exec_depend>message_runtime</exec_depend>\n \0,g' package.xml

	cat << EOF >> CMakeLists.txt
add_executable(server src/server.cpp)
target_link_libraries(server \${catkin_LIBRARIES})
add_dependencies(server ${PACKAGE_NAME}_gencpp)

add_executable(client src/client.cpp)
target_link_libraries(client \${catkin_LIBRARIES})
add_dependencies(client ${PACKAGE_NAME}_gencpp)
EOF

	return $?
}

install_sources() {

	mkdir $CATKIN_WS/src/$PACKAGE_NAME/scripts -p
	for script in `ls /tmp/scripts/*.py`; do
		ln -s $script $CATKIN_WS/src/$PACKAGE_NAME/scripts
	done
	for script in `ls /tmp/scripts/*.cpp`; do
		ln -s $script $CATKIN_WS/src/$PACKAGE_NAME/src
	done
}

install_ws() {
	cd "$CATKIN_WS"
	catkin_make install >/dev/null
}

main() {

	echo "Initializing ..."
	init

	echo "Creating package '$PACKAGE_NAME' ..."
	create_package

	echo "Creating service '$SERVICE_NAME' & configuring catkin ..."
	create_service

	echo "Installing scripts & sources ..."
	install_sources

	echo "Installing workspace ..."
	install_ws

	echo "Starting ROS core ..."
	roscore > /var/log/roscore &
	sleep 4


	echo "Compiling python to bytecode (which would either way be run after first run)"
	roscd $PACKAGE_NAME
	python -m py_compile scripts/client.py scripts/server.py

	echo "Starting $PACKAGE_NAME Python server ..."
	rosrun $PACKAGE_NAME server.py &

	sleep 2

	echo 
	echo " --- Starting benchmarking ROS Python ! ---"
	echo 

	rosrun $PACKAGE_NAME client.py /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client.py /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client.py /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client.py /tmp/img/medium.jpg
	rosrun $PACKAGE_NAME client.py /tmp/img/medium.jpg
	rosrun $PACKAGE_NAME client.py /tmp/img/medium.jpg

	echo
	echo "Starting $PACKAGE_NAME C++ server ..."
	rosrun $PACKAGE_NAME server &

	sleep 2

	echo 
	echo " --- Starting benchmarking ROS C++ ! ---"
	echo

	rosrun $PACKAGE_NAME client /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client /tmp/img/large.jpg
	rosrun $PACKAGE_NAME client /tmp/img/medium.jpg
	rosrun $PACKAGE_NAME client /tmp/img/medium.jpg
	rosrun $PACKAGE_NAME client /tmp/img/medium.jpg

	echo "I'm done!"
	return $?
}

main $@
exit $?
