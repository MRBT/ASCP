#include "ros/ros.h"
#include <fstream>
#include <chrono>
#include "cv_bridge/cv_bridge.h"

#include "image_benchmark/BounceImage.h"
#include <opencv2/highgui/highgui.hpp>

#include <experimental/filesystem>

std::string basename(const std::string &path) {
    auto found = static_cast<unsigned int>(path.find_last_of("/\\"));
    return path.substr(found + 1);
}

typedef uint8_t BYTE;

/**
 *      See https://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29 for explanation
 *
 * @param image
 * @param buffer
 */
void send_image(const std::string &filename, sensor_msgs::ImagePtr *image) {

    using namespace std::chrono;

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<image_benchmark::BounceImage>("image_bounce_cpp");
    image_benchmark::BounceImage service;

    long double sent_at = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;

    service.request.request_time = static_cast<double>(sent_at);
    service.request.request_image = *(*image);

    // call service
    if (!client.call(service)) {
        ROS_ERROR("Failed to call service BounceImage");
        return;
    }

    long double sent_back_at = service.response.response_time;
    long double returned_at = duration_cast<microseconds>(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;

    // output results
    // The calculations are a subject for optimalization .. but from here we basically don't care about the time .. all is already measured
    std::cout << "TOF ("
              << basename(filename)
              << '/'
              << (*image)->data.size() / 1000
              << "kB"
              << "): \t"
              << boost::format("%05.2f") % ((returned_at - sent_at) * 1000.0)
              << " ms (client ->("
              << boost::format("%05.2f") % ((sent_back_at - sent_at) * 1000.0)
              << "ms)-> server ->("
              << boost::format("%05.2f") % ((returned_at - sent_back_at) * 1000.0)
              << "ms)-> client) "
              << '\n';

}



int main(int argc, char **argv) {
    // check correct num of args
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " IMAGE_PATH " << '\n';
        return 1;
    }

    ros::init(argc, argv, "image_bounce_client");

    std::string filename = argv[1];

    // cv::Mat image = cv::imread(filename);
    // sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();


    // std::cout << "Opening file .." << '\n';

    // open the file:
    std::ifstream file(filename, std::ios::binary);

    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);

    // get its size:
    std::streampos fileSize;

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // std::cout << "Read file .. " << fileSize/1000 << " kB" << '\n';

    sensor_msgs::ImagePtr image_ptr = boost::make_shared<sensor_msgs::Image>();

    // std::cout << "Inserting data .." << '\n';
    // ptr->data.insert(ptr->data.begin(),
    //            std::istream_iterator<BYTE>(file),
    //            std::istream_iterator<BYTE>()
    // );

    std::copy(std::istream_iterator<BYTE>(file),
              std::istream_iterator<BYTE>(),
              std::back_inserter(image_ptr->data));

    std_msgs::HeaderPtr header_ptr = boost::make_shared<std_msgs::Header>();

    // We need to initialize as much data as in Python by default so that the data being sent back & forth are comparable ...
    header_ptr->stamp = ros::Time();
    header_ptr->frame_id = "";
    header_ptr->seq = 0;

    image_ptr->header = *header_ptr;

    image_ptr->height = 0;
    image_ptr->width = 0;
    image_ptr->is_bigendian = 0;
    image_ptr->step = 0;
    image_ptr->encoding = "";

    // std::cout << "Data  inserted .." << '\n';


    send_image(filename, &image_ptr);

    return 0;
}
