#!/usr/bin/env python

from datetime import datetime
from image_benchmark.srv import *
import rospy

def handle_image_bounce(req):

    then = req.request_time
    now = float(datetime.now().strftime("%s.%f"))

    print("Recieving image took {} seconds.".format(now - then))

    return BounceImageResponse(now, req.request_image)

def image_bounce_server():
    rospy.init_node('image_bounce_server_python')
    s = rospy.Service('image_bounce_python', BounceImage, handle_image_bounce)
    print "Ready to bounce images ... "
    rospy.spin()

if __name__ == "__main__":
    image_bounce_server()
