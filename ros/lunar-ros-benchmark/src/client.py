#!/usr/bin/env python

import sys
import rospy
import os
from sensor_msgs.msg import Image
from image_benchmark.srv import *

from datetime import datetime


def send_image(filename, image):
    rospy.wait_for_service('image_bounce_python')
    try:
        bounce = rospy.ServiceProxy('image_bounce_python', BounceImage)
        sent_at = float(datetime.now().strftime("%s.%f"))
        resp1 = bounce(sent_at, image)
        sent_back_at = resp1.response_time
        returned_at = float(datetime.now().strftime("%s.%f"))

        # The calculations are a subject for optimalization .. but from here we basically don't care about the time .. all is already measured
        print("TOF ({}/{}kB): \t{:05.2f} ms (client ->({:05.2f}ms)-> server ->({:05.2f}ms)-> client)".format(
            os.path.basename(filename),
            len(image.data) / 1000,
            (returned_at - sent_at) * 1000,
            (sent_back_at - sent_at) * 1000,
            (returned_at - sent_back_at) * 1000,
        ))

    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def usage():
    return "%s [image_path]" % sys.argv[0]


if __name__ == "__main__":
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        print usage()
        sys.exit(1)

    try:
        with open(filename, mode='rb') as file:
            image = Image(data=file.read())
    except IOError:
        print "Cannot read '{}' file - does it exist? Do I have privileges?".format(filename)
        sys.exit(2)

    send_image(filename, image)
