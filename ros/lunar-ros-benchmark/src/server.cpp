#include "ros/ros.h"
#include "image_benchmark/BounceImage.h"
#include <chrono>

bool bounce(image_benchmark::BounceImage::Request &req,
            image_benchmark::BounceImage::Response &res) {

    using namespace std::chrono;

    double seconds = duration_cast< microseconds >(
            system_clock::now().time_since_epoch()
    ).count() / 1e6;

    double then = req.request_time;

    ROS_INFO("Server - Current time: %f s", seconds);
    ROS_INFO("Then time: %f s", then);
    ROS_INFO("Receiving image took %f s", seconds - then);

    res.response_time = static_cast<double>(seconds);
    res.response_image = req.request_image;
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "image_bounce_server_cpp");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("image_bounce_cpp", bounce);
    ROS_INFO("Ready to bounce images ...");
    ros::spin();

    return 0;
}
