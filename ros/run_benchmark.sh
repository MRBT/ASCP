#!/bin/bash

if test -z "`which docker-compose`"; then
    echo "You first have to install docker & docker-compose!"
    echo
    echo " See this link:"
    echo "  - https://docs.docker.com/compose/install/#prerequisites"
    echo
    exit 1
fi

# We don't need X11 for this ..
# xhost +local:ros-tutorial-docker >/dev/null

docker-compose up ros-benchmark
