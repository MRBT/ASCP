#!/bin/bash

if test -z "`which docker-compose`"; then
    echo "You first have to install docker & docker-compose!"
    echo
    echo " See this link:"
    echo "  - https://docs.docker.com/compose/install/#prerequisites"
    echo
    exit 1
fi

# Allow to get X11 from within the container
xhost +local:ros-tutorial-docker >/dev/null

docker-compose up -d ros-tutorial

echo
echo "Attaching you to the 'ros-tutorial' bash ..."
echo

docker-compose exec ros-tutorial bash
